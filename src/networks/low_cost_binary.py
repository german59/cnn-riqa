__author__ = ["Germán Pinedo"]
__copyright__ = "Copyright 2022, Germán Pinedo - CINVESTAV UNIDAD GUADALAJARA"
__credits__ = ["German Pinedo"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = ["German Pinedo"]
__email__ = "german.pinedo@cinvestav.mx"
__status__ = "Beta"

from keras.layers import Input, Dense, Flatten, MaxPooling2D, Conv2D, BatchNormalization, Dropout
from keras.models import Model
from contextlib import redirect_stdout

class QRetiNetv1(Model):
    def __init__(self, imgzs, n_classes, dropout_prob):
        super().__init__()
        self.convIn = Conv2D(filters=16, kernel_size=(3, 3), strides=(2, 2), activation='relu', padding='valid', name='convIn')
        self.conv1A = Conv2D(filters=32, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='conv1a')
        self.conv2A = Conv2D(filters=32, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='conv2a')
        self.conv1B = Conv2D(filters=64, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='conv1b')
        self.conv2B = Conv2D(filters=64, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='conv2b')
        self.conv1C = Conv2D(filters=128, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='conv1c')
        self.convOut = Conv2D(filters=128, kernel_size=(3, 3), strides = (2, 2), activation='relu', padding='valid', name='convOut')
        self.n_classes = n_classes
        self.dropout_prob = dropout_prob
        self.imgzs = imgzs
    @property
    def __name__(self):
        return "low_cost_q"

    def call(self, inputs):
        x = self.convIn(inputs)
        x = self.conv1A(x)
        x = self.conv2A(x)
        x = self.conv1B(x)
        x = self.conv2B(x)
        x = self.conv1C(x)
        x = self.conv2C(x)
        x = self.convOut(x)
        x = Flatten()(x)
        x = Dense(32, activation="relu")(x)
        x = Dense(self.n_classes, activation="sigmoid")(x)
        return x

    def model_summary(self, print_fn):
        inputs = Input(shape=(self.imgzs, self.imgzs, 3))
        outputs = self.call(inputs)
        Model(inputs=inputs, outputs=outputs, name=self.__name__).summary(print_fn=print_fn)
