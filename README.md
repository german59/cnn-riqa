# Retinal Image Quality Assesment with CNN

There are models to classify the level of diabetic retinopathy disease, 
because the medical application demands a low error rate, it has been 
verified that in image training with poor quality it tends to have a 
lower sensitivity specificity than expected, it is due to that is why 
this project provides a pre-analysis of the images to ensure a good 
retinal image to be pre-diagnosed by an artificial intelligence system.

![alttext](images/example_1.jpg "Good quality")
VS
![alttext](images/example_0.jpg "Bad quality")
## Pre-requisites 📋
```
$ git clone https://gitlab.com/german59/cnn-riqa.git
```
```
Python 3.7
Keras 2.3.1
Tensorflow-gpu 1.15.0
OpenCV 4.1.2
Sickit-learn 0.22.1
Seaborn 0.10
h5py 2.10
Numpy 1.18
Pandas 1.03
Matplotlib 3.1.2
```
The dataset used for this project is obtained from public datasets

* [**KAGGLE**](https://www.kaggle.com/c/diabetic-retinopathy-detection/data) - Kaggle competition
* [**IDRID**](https://idrid.grand-challenge.org/Data/) - Indian Diabetic Retinopathy Image Dataset
* [**DRIVE**](https://drive.grand-challenge.org/Download/) - Digital Retinal Images for Vessel Extraction

Later images are cropped to have only the region of interest to have a homogeneous dataset:

<p align="center">
    <img src="images/example_crop.jpeg" alt="crop" width="250"/>
</p>

Data are analyzed with quality indicators using processing to measure quality indicators in images and determine a first label of good or bad quality acording to:

* [**Quality indicators**](https://gitlab.com/german59/retinal-image-quality-assesment) - Image quality assesment by quality indicators

### Files and Directories


- **main_train.py:** Training mode

- **models:** All models used

- **src:** Scripts for evaluate, test, analize, visualization and Gradient-CAM, 

- **Results:** Trained, evaluated, tested, history and confusion matrix for each model

## Training models

The only thing you have to do to get started is set up the folders in the following structure
(Min=800 in train folder):

    ├── "dataset"                   
    |   ├── train
    |   |   ├── 0
    |   |   ├── 1
    |   ├── val
    |   |   ├── 0
    |   |   ├── 1
    |   ├── test
    |   |   ├── 0
    |   |   ├── 1

Models used for this project are declared in **main_train.py**:
```
from models.ResNet50 import ResNet_model
from models.RetiNet import RetiNet_model
from models.Inception_v3 import Inception_v3_model
from models.Inception_v4 import Inception_v4_model
from models.MobileNet import MobileNet_model
from models.AlexNet import AlexNet_model
```
Configuration of training parameters, directory, times, learning rate, etc.:
```
train_data_dir = 'dataset/train'
validation_data_dir = 'dataset/val'
nb_train_samples = 1600
nb_validation_samples = 400
epochs = 100
lr = 0.0001
```

Depending on the capacity of the training system, the batch size is configured and the training model is called:
```
#RetiNet Custom
model, Name_model, img_width, img_height = RetiNet_model('add name')
batch_size = 32 
```
```
# ResNet 50
model, Name_model, img_width, img_height = ResNet_model('add name')
batch_size = 32
```

Finally, a training function is called with its parameters:
```
train_models(model, Name_model,
                    img_width, img_height,
                    epochs, batch_size, lr,
                    train_data_dir, validation_data_dir,
                    nb_train_samples, nb_validation_samples)
```
## Train, test and analysis results

<img src="/Train results/Train/Inception_v4/D2/Inception_v4_Final_D2_acc.png" alt="accuracy" width="400"/>
<img src="/Train results/Train/Inception_v4/D2/Inception_v4_Final_D2_loss.png" alt="loss" width="400"/>

|      Model      | Accuracy Train | Loss  Train | Accuracy Val  | Loss Val  |
|:---------------:|:--------------:|:-----------:|:-------------:|:---------:|
| Inception v3 D1 |    0.9896      |   0.0180    |     0.9702    |   0.0022  |
| Inception v3 D2 |    0.9973      |   0.0111    |     0.9632    |   0.0086  |
| Inception v4 D1 |    0.9875      |   0.0016    |     0.9848    |   0.0035  |
| **Inception v4 D2** |    **0.9962**      |   **0.0097**    |     **0.9877**    |   **0.0025**  |
| ResNet 50 D1    |    0.9895      |   0.0095    |     0.9723    |   0.0022  |
| ResNet 50 D2    |    0.9901      |   0.0046    |     0.9801    |   0.0018  |
| RetiNet D1      |    0.9756      |   0.0739    |     0.9572    |   0.0108  |
| RetiNet D2      |    0.9931      |   0.0208    |     0.9775    |   0.0265  |

### Evaluation Results 

|      Model      | Accuracy  Evaluation | Loss  Evaluation |
|:---------------:|:--------------------:|:----------------:|
| Inception v3 D1 |        0.9474        |      0.1628      |
| Inception v3 D2 |        0.9850        |      0.0031      |
| Inception v4 D1 |        0.9100        |      0.0005      |
| **Inception v4 D2** |        **0.9925**        |      **0.0044**      |
| ResNet 50 D1    |        0.8999        |      0.2878      |
| ResNet 50 D2    |        0.9915        |      0.0094      |
| RetiNet D1      |        0.9725        |      0.0508      |
| RetiNet D2      |        0.9900        |      0.0071      |

### Confusion Matrix

<img src="/Train results/conf_matrix/Inception_v4_D2_CM.png" alt="Confusion Matrix" width="400"/>

### ROC Curve and AUC Results

<img src="/Train results/roc-auc/Inception_v4_D2_ROC-AUC.png" alt="ROC-AUC-I4D2" width="500"/>

| Model        |AUC D1 |AUC D2 |AUC D3|
|--------------|:-----:|:-----:|:----:|
| Inception v3 | 0.990 | 0.997 |      |
| Inception v4 | 0.962 | 0.992 |      |
| ResNet 50    | 0.978 | 0.991 |      |
| RetiNet      | 0.991 | 0.995 |      |



### Sensitivity and specifity 

|      Model      | Sensitivity(%)| Specifity(%)|
|:---------------:|:-------------:|:-----------:|
| Inception v3 D1 |     93        |    96.5     |
| Inception v3 D2 |     98        |    98       |
| Inception v4 D1 |     93        |    89       |
| **Inception v4 D2**|  **99**    | **99.5**    |
| ResNet 50 D1    |     96        |    84       |
| ResNet 50 D2    |     99        |    98       |
| RetiNet D1      |     97.5      |    97       |
| RetiNet D2      |     99.5      |    95.5     |

### Performance

|     Modelo   |Timeo of train  (Hrs)|No. of params  (Mill.)|Time of infer (seg)|Memory usage  (MB)|
|:------------:|:-------------------------------:|:---------------------------:|:---------------------------:|:---------------------:|
| Inception v3 |               1.21              |            155.98           |                             |          703          |
| Inception v4 |               2.29              |            154.94           |                             |          880          |
| ResNet 50    |               1.04              |            126.29           |                             |          609          |
| RetiNet      |               1.79              |            17.11            |                             |          162          |

### Gradient CAM heatmap

```
Class 0:
```

<img src="/images/grad_cam_class_0.png" alt="grad-cam-0" width="700"/>

```
Class 1:
```

<img src="/images/grad_cam_class_1.png" alt="grad-cam-1" width="700"/>

## Licence 📄

Academic Free License v3.0

